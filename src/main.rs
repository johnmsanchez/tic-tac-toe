use std::io::{self, Write};

const PLAYER_O: char = 'O';
const PLAYER_X: char = 'X';
const MAX_TURNS: u16 = 9;

enum GameState {
    Tie,
    Win,
}

fn print_matrix(matrix: [char; 9]) {
    println!(
    "
     {}  |  {}  |  {} 
     _  _  _  _  _ \n
     {}  |  {}  |  {} 
     _  _  _  _  _ \n
     {}  |  {}  |  {} 
    ",
        matrix[0], matrix[1], matrix[2],
        matrix[3], matrix[4], matrix[5],
        matrix[6], matrix[7], matrix[8]
    );
}

fn viable_win_condition(matrix: [char; 9]) -> bool {
    matches!(
        matrix,
        ['X', 'X', 'X', _, _, _, _, _, _]
        | [_, _, _, _, _, _, 'X', 'X', 'X']
        | ['X', _, _, 'X', _, _, 'X', _, _]
        | [_, _, 'X', _, _, 'X', _, _, 'X']
        | [_, 'X', _, _, 'X', _, _, 'X', _]
        | [_, _, _, 'X', 'X', 'X', _, _, _]
        | ['X', _, _, _, 'X', _, _, _, 'X']
        | [_, _, 'X', _, 'X', _, 'X', _, _]
        | ['O', 'O', 'O', _, _, _, _, _, _]
        | [_, _, _, _, _, _, 'O', 'O', 'O']
        | ['O', _, _, 'O', _, _, 'O', _, _]
        | [_, _, 'O', _, _, 'O', _, _, 'O']
        | [_, 'O', _, _, 'O', _, _, 'O', _]
        | [_, _, _, 'O', 'O', 'O', _, _, _]
        | ['O', _, _, _, 'O', _, _, _, 'O']
        | [_, _, 'O', _, 'O', _, 'O', _, _]
    )
}

fn update_matrix(matrix: &mut [char; 9], position: usize, player: char) {
    matrix[position] = player;
}

fn update_player(player: &mut char) {
    match *player {
        PLAYER_O => {
            *player = PLAYER_X;
        }
        PLAYER_X => {
            *player = PLAYER_O;
        }
        _ => {}
    }
}

fn main() -> io::Result<()> {
    let stdin = io::stdin();
    let mut user_input = String::new();
    let mut user_num: usize;

    let mut player: char = PLAYER_X;
    let mut turns: u16 = 0;
    let state: GameState;

    let mut matrix = [
        '1', '2', '3', 
        '4', '5', '6', 
        '7', '8', '9'
    ];

    println!("Welcome To Tic-Tac-Toe!");
    print_matrix(matrix);

    loop {
        print!("Player {}, enter a number between 1 and 9: ", player);

        io::stdout().flush()?;
        stdin.read_line(&mut user_input)?;

        match user_input.trim().parse::<u16>() {
            Ok(num) => {
                if !(1..=9).contains(&num) {
                    println!("Number must be between 1 and 9!");
                    continue;
                }
                user_num = num as usize;
            }
            Err(err) => {
                println!("Error: {}", err);
                continue;
            }
        }

        update_matrix(&mut matrix, user_num - 1, player);
        turns += 1;

        println!();
        println!("------------------------------------------");
        println!("Current Matrix");
        print_matrix(matrix);

        if turns > 4 && viable_win_condition(matrix) {
            state = GameState::Win;
            break;
        }

        if turns == MAX_TURNS {
            state = GameState::Tie;
            break;
        }

        update_player(&mut player);
        user_input = String::new();
    }

    match state {
        GameState::Win => println!("Player {} wins!", player),
        GameState::Tie => println!("Tie! No one wins."),
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn update_matrix_correctly() {
        let player = PLAYER_O;
        let position = 0;
        let mut test_matrix = [
            '1', '2', '3', 
            '4', '5', '6', 
            '7', '8', '9'
        ];

        update_matrix(&mut test_matrix, position, player);
        assert_eq!(test_matrix, [
            'O', '2', '3', 
            '4', '5', '6', 
            '7', '8', '9'
        ]);
    }

    #[test]
    fn win_condition_returns_true() {
        let test_matrix = [
            'X', 'X', 'X', 
            '4', '5', '6', 
            '7', '8', '9'
        ];

        assert_eq!(viable_win_condition(test_matrix), true);
    }

    #[test]
    fn non_win_condition_returns_false() {
        let test_matrix = [
            '1', '2', '3', 
            '4', '5', '6', 
            '7', '8', '9'
        ];

        assert_eq!(viable_win_condition(test_matrix), false);
    }

    #[test]
    fn update_player_correctly() {
        let mut player = PLAYER_O;

        update_player(&mut player);
        assert_eq!(player, PLAYER_X);
    }
}
