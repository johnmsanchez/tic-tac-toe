# tic-tac-toe
Tic-Tac-Toe implemented in Rust.

## Screenshots
<div align="center">
  <img height="240" src="https://user-images.githubusercontent.com/48599206/203369464-6f3a073c-c3fe-41a2-8575-b137d5b5e5e1.png" />
  <img height="240" src="https://user-images.githubusercontent.com/48599206/203370369-e03bd600-4af2-4ab7-802d-043ea7730f4e.png" />
</div>
